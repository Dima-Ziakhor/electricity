# Logic

There is a set of Power Plants and a set of Households. Every Household can be
connected to any number of Power Plants. Power Plant feeds the Household with the
Electricity. The Household has Electricity if it's connected to one or more
Power Plants.

Each Power Plant is alive by default, but can be killed. The Power Plant which
is not Alive will not generate any Electricity.

Household can be connected to Household. The Household which has the Electricity
also passes it to all the connected Households.

The Power Plant can be repaired after killed.

# Setup

### install dependencies:

```shell
npm i
```

### Once you are ready to test your solution please run

```shell
npm test
```

# Usage

- Create new object World
- Use createPowerPlant method to create new power plant
- Use createHousehold method to create new household
- Use connectHouseholdToPowerPlant method to connect the household to the power plant
- Use connectHouseholdToHousehold method to connect the household to the another household
- Use disconnectHouseholdFromPowerPlant method to disconnect the household from the power plant
- Use killPowerPlant method to mess up the power plant (it will not be able to produce electricity)
- Use repairPowerPlant method to repair the power plant after breakdown
- Use householdHasEletricity method to check the household has eletricity
- checkHouseholdConnection - utility method. You don't need to use it directly. It is used to test the household connection with power plants and other households.
