 export class World {
  constructor() {
    this.powerPlants = [];
    this.households = [];
  }

  createPowerPlant() {
    const powerPlant = {
      name: 'power plant',
      isAlive: true,
      connectToHouseholds: [],
    };

    this.powerPlants.push(powerPlant);
    return powerPlant;
  }

  createHousehold() {
    const household = {
      name: 'household',
      hasEletricity: false,
      connectToPowerPlants: [],
      connectToHouseholds: [],
    };

    this.households.push(household);
    return household;
  }

  connectHouseholdToPowerPlant(household, powerPlant) {
    household.hasEletricity = powerPlant.isAlive;
    household.connectToPowerPlants.push(powerPlant);
    powerPlant.connectToHouseholds.push(household);
  }

  connectHouseholdToHousehold(household1, household2) {
    household1.connectToHouseholds.push(household2);
    household2.connectToHouseholds.push(household1);

    if (household1.hasEletricity || household2.hasEletricity) {
      household1.hasEletricity = true;
      household2.hasEletricity = true;
    }
  }

  disconnectHouseholdFromPowerPlant(household, powerPlant) {
    const indexOfpowerPlant = household.connectToPowerPlants
      .findIndex(currentPowerPlant => currentPowerPlant === powerPlant);

    if (indexOfpowerPlant !== -1) {
      household.connectToPowerPlants.splice(indexOfpowerPlant, 1);
      household.hasEletricity = this.checkHouseholdConnection(household);
    }
  }

  killPowerPlant(powerPlant) {
    powerPlant.isAlive = false;

    powerPlant.connectToHouseholds
      .forEach(currentHousehold => currentHousehold.hasEletricity = this.checkHouseholdConnection(currentHousehold));
  }

  repairPowerPlant(powerPlant) {
    powerPlant.isAlive = true;

    powerPlant.connectToHouseholds
      .forEach(currentHousehold => currentHousehold.hasEletricity = this.checkHouseholdConnection(currentHousehold));
  }

  householdHasEletricity(household) {
    return household.hasEletricity;
  }

  checkHouseholdConnection(household) {
    const alivePowerPlant = household.connectToPowerPlants
      .find(currentPowerPlant => currentPowerPlant.isAlive === true);

    if (alivePowerPlant) {
      return true;
    }

    let connectedHousehold = false;

    this.households.forEach(currentHousehold => {
      if (currentHousehold.hasEletricity
        && currentHousehold.connectToHouseholds.includes(household)
      ) {
        connectedHousehold = true;
      }
    });

    return connectedHousehold;
  }
}
